//this is for question 2 : area of a disk
#include <stdio.h>
int main()
{
    float r,pi,Area;
    printf("Enter value for Pi(3 or 3.14): ");
    scanf("%f",&pi);
    printf("Enter value for r: ");
    scanf("%f",&r);
    Area = pi * r * r;
    printf("Area of the disk is %.3f  ",Area);

    return 0;

}
